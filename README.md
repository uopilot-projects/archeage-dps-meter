ДПС-Метр
========

Hебольшой пример работы программы

[Видео на YouTube](https://www.youtube.com/watch?v=D_I38gsocuQ)


Макрос программы UoPilot для игры Archeage для замера скорости убийства монстров

    // дпс метр
    log clear
    log open
    set #color1 2833556  // цвет начала полоски хп
    set #color2 3228839 // конец полоски хп
    set #color3 3492792 // альтернативное начало полоски хп
    set #x1 790
    set #y1 0
    set #x2 1240
    set #y2 80
    set #N 0
    set #sum 0
    while 1 = 1
        set #N (#N + 1)
        set #b 0
    while #b = 0
        set #b findcolor (#x1 #y1 #x2 #y2 1 1 #color2 %arr1 2 abs)
    end_while
    set #a findcolor (#x1 #y1 #x2 #y2 1 1 #color1 %arr2 2 abs)
    if #a = 0
        set #a findcolor (#x1 #y1 #x2 #y2 1 1 #color3 %arr2 2 abs)
        set #color1 #color3
    end_if
    sort_array %arr1
    sort_array %arr2
    while %arr1[#b 1], %arr1[#b 2] #color2
        wait 1
    end_while
    set timer
    while %arr2[1 1], %arr2[1 2] #color1
        wait 1
    end_while
    set #time timer
    log Time: #time
    set #sum (#sum + #time)
    set #avg (#sum / #N)
    log Average: #avg
    log -------------
    end_while
    end_script